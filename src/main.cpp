#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iostream>
#include <string>
#include <vector>

#include "PatranObserver.h"
#include "PatranLoader.h"
#include "GenerateCpp.h"

/**
 * Function to validate the input type.
 */
bool valid_intype(const std::string & intype) 
{
  if (intype.compare("patran") == 0) {
    return true;
  }
  if (intype.compare("tecplot") == 0) {
    return true;
  }
  return false;
}

/**
 * Function to validate the output type.
 */
bool valid_outtype(const std::string & outtype)
{
  if (outtype.compare("c++") == 0) {
    return true;
  }
  return false;
}

/**
 * Patran observer implementation.
 */
class PatranObserverImpl : public PatranObserver
{
public:
  PatranObserverImpl(
      std::vector< std::vector<double> > & vertices,
      std::vector< std::vector<int>    > & elements
  ) : mVertices(vertices), mElements(elements)
  {}

  virtual void vertex(double x, double y, double z)
  {
    mVertices.push_back({x, y, z});
  }

  virtual void element(int idx, int sidx, int n0, int n1, int n2, int n3)
  {
    mElements.push_back({idx, sidx, n0, n1, n2, n3});
  }
private:
  std::vector< std::vector<double> > & mVertices;
  std::vector< std::vector<int>    > & mElements;
};

/**
 * Main function.
 */
int main(int argc, char **argv)
{
  // Read inputs from command line
  if (argc != 5) {
    std::cout << "Required parameters:\n";
    std::cout << "   $> ./gencode <infile> <intype> <outfile> <outtype>\n";
    std::cout << "where <intype> is one of:\n";
    std::cout << "   patran\n";
    std::cout << "   tecplot\n";
    std::cout << "and <outtype> is one of:\n";
    std::cout << "   c++\n";
    exit(-1);
  }

  std::string infile(argv[1]);
  std::string intype(argv[2]);
  std::string outfile(argv[3]);
  std::string outtype(argv[4]);

  // Validate inputs
  if (!valid_intype(intype)) {
    std::cout << "Input type '" << intype << "' not valid\n";
    exit(-1);
  }

  if (!valid_outtype(outtype)) {
    std::cout << "Output type '" << outtype << "' not valid\n";
    exit(-1);
  }

  std::string className = outfile;
  std::replace(className.begin(), className.end(), '.', '_');

  /////////////////////////////////////////////////////////////////////////////
  // Input patran, output c++                                                //
  /////////////////////////////////////////////////////////////////////////////
  if ( (intype.compare("patran") == 0) && (outtype.compare("c++") == 0) ) {
    std::vector< std::vector<double> > vertices;
    std::vector< std::vector<int>    > elements;

    // Input
    PatranObserverImpl observer(vertices, elements);
    PatranLoader loader(observer);
    loader.load(infile);

    // Output
    GenerateCpp cppGen;
    
    std::ofstream fout;
    fout.open(outfile);

    fout << cppGen.generate(className, vertices, elements);

    fout.close();
  }

  /*
  for (auto v : vertices) {
    std::cout 
      << std::right << std::fixed << std::setw(11) << std::setprecision(6) << v[0] << ", "
      << std::right << std::fixed << std::setw(11) << std::setprecision(6) << v[1] << ", "
      << std::right << std::fixed << std::setw(11) << std::setprecision(6) << v[2] 
      << std::endl;
  }

  for (auto e : elements) {
    std::cout 
      << "value @"
      << std::left  << std::setw(4) << e[0] << " "
      << "[mat: " << std::right << std::setw(4) << e[1] << "] "
      << std::right << std::setw(4) << e[2] << ", "
      << std::right << std::setw(4) << e[3] << ", "
      << std::right << std::setw(4) << e[4] << ", "
      << std::right << std::setw(4) << e[5]
      << std::endl;
  }
  */
}

