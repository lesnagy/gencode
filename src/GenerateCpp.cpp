#include <sstream>
#include <iomanip>

#include "GenerateCpp.h"

std::string GenerateCpp::generate(const std::string                        & className,
                                  const std::vector< std::vector<double> > & vertices,
                                  const std::vector< std::vector<int>    > & elements)
{
  size_t nvertices = vertices.size();
  size_t nelements = elements.size();

  std::stringstream ss;

  ss << "class " << className << "\n";
  ss << "{\n";
  ss << "public:\n";
  ss << "  unsigned long nvert;\n";
  ss << "  unsigned long nelem;\n";
  ss << "  double   x[" << nvertices << "];\n";
  ss << "  double   y[" << nvertices << "];\n";
  ss << "  double   z[" << nvertices << "];\n";
  ss << "  int     id[" << nelements << "];\n";
  ss << "  int    mid[" << nelements << "];\n";
  ss << "  int     n0[" << nelements << "];\n";
  ss << "  int     n1[" << nelements << "];\n";
  ss << "  int     n2[" << nelements << "];\n";
  ss << "  int     n3[" << nelements << "];\n";
  ss << "\n";
  ss << "  " << className << "() {\n";
  ss << "    nvert = " << nvertices << ";\n";
  ss << "    nelem = " << nelements << ";\n";
  ss << "\n";
  for (size_t i = 0; i < vertices.size(); ++i) {
  auto v = vertices[i];
  ss << "    x[" << std::right << std::setw(3) << i << "] = " 
    << std::right << std::fixed << std::setw(9) << std::setprecision(6) << v[0] << "; ";
  ss <<     "y[" << std::right << std::setw(3) << i << "] = " 
    << std::right << std::fixed << std::setw(9) << std::setprecision(6) << v[1] << "; ";
  ss <<     "z[" << std::right << std::setw(3) << i << "] = " 
    << std::right << std::fixed << std::setw(9) << std::setprecision(6) << v[2] << ";\n";
  }
  ss << "\n";
  for (size_t i = 0; i < elements.size(); ++i) {
  auto e = elements[i];
  ss << "    id[" << std::right << std::setw(4) << i << "] = " 
    << std::right << std::fixed << std::setw(4) << e[0] << "; ";
  ss <<    "mid[" << std::right << std::setw(4) << i << "] = " 
    << std::right << std::fixed << std::setw(4) << e[1] << "; ";
  ss <<     "n0[" << std::right << std::setw(4) << i << "] = " 
    << std::right << std::fixed << std::setw(4) << e[2] << "; ";
  ss <<     "n1[" << std::right << std::setw(4) << i << "] = " 
    << std::right << std::fixed << std::setw(4) << e[3] << "; ";
  ss <<     "n2[" << std::right << std::setw(4) << i << "] = " 
    << std::right << std::fixed << std::setw(4) << e[4] << "; ";
  ss <<     "n3[" << std::right << std::setw(4) << i << "] = " 
    << std::right << std::fixed << std::setw(4) << e[5] << ";\n";
  }
  ss << "  }\n";
  ss << "};\n";

  return ss.str();
}

