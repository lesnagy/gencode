#ifndef GENERATE_CPP_H_
#define GENERATE_CPP_H_

#include <string>
#include <vector>

class GenerateCpp
{
public:
  std::string generate(const std::string                        & className,
                       const std::vector< std::vector<double> > & vertices,
                       const std::vector< std::vector<int>    > & elements);
};

#endif  // GENERATE_CPP_H
